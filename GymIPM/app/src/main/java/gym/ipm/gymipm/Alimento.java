package gym.ipm.gymipm;

import com.orm.SugarRecord;

/**
 * Created by Douglas on 23/11/2016.
 */

public class Alimento extends SugarRecord {

    String name;
    float calorias;
    float hidratos;
    float proteinas;
    float lipidos;

    public Alimento(){

    }

    public Alimento(String name, float calorias, float hidratos, float proteinas, float lipidos){
        this.name = name;
        this.calorias = calorias;
        this.hidratos = hidratos;
        this.proteinas = proteinas;
        this.lipidos = lipidos;

    }

    public float getHidratos() {
        return hidratos;
    }

    public float getCalorias() {
        return calorias;
    }

    public float getLipidos() {
        return lipidos;
    }

    public float getProteinas() {
        return proteinas;
    }

    public String getName() {
        return name;
    }

}

