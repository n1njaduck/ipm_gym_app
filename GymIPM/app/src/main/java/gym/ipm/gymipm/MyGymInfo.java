package gym.ipm.gymipm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class MyGymInfo extends Fragment {
    public MyGymInfo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.mygym_video360_fragment, container, false);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.mygym_info_fragment, container, false);
    }
}
