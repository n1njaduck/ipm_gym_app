package gym.ipm.gymipm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Evento_AulaGrupo extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento__aula_grupo);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Detalhe Aula grupo");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Eventos_calendario.class);
                startActivity(i);
            }
        });

        Intent intent = getIntent();
        Long id = getIntent().getLongExtra("id", 0);

        final CalendarEvents ce = CalendarEvents.findById(CalendarEvents.class, id);

        Log.d("CE", ce.getType() );


        Button button = (Button) findViewById(R.id.unsubscribe_button);

        // dialog
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder( Evento_AulaGrupo.this);

                builder.setMessage("Confirma a desinscrição?")
                        .setPositiveButton("Sim",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                CalendarEvents.delete(ce);
                                Intent i = new Intent(getApplicationContext(), Eventos_calendario.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })
                        .show();


            }
        });
    }
}
