package gym.ipm.gymipm;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Dashboard_Nutricao extends Fragment {


    private PieChart mChart;

    private String [] types_nutricao = new String[]{ "Hidratos", "Proteínas", "Lipídos"};
    private TextView calorias_count;
    private ListView listView;


    public Dashboard_Nutricao() {
        
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard__nutricao, container, false);

        listView = (ListView) view.findViewById(R.id.list_de_nutricao);

        ArrayList<String> values2 = new ArrayList<>();

        List<Alimento> ali = Alimento.listAll(Alimento.class);

        for(int i=0; i< ali.size(); i++){

            String n = ali.get(i).getName() + " " + ali.get(i).getCalorias() + " Kcal";
            Log.d("ALIMENTO", n);
            values2.add(n);
        }





        ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, values2);

        listView.setAdapter(adapter);







        calorias_count = (TextView) view.findViewById(R.id.textView30);


        mChart = (PieChart) view.findViewById(R.id.chart1);

        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);

        setData(4, 100);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutBack);


        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);


        return view;
    }

    private void setData(int count, float range) {

        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();


        List<Alimento> cal_events = Alimento.listAll(Alimento.class);
        int calorias=0;
        int hidratos=0;
        int proteinas=0;
        int lipidos=0;

        for(Alimento ce: cal_events){
            calorias+= ce.getCalorias();
            hidratos+= ce.getHidratos();
            proteinas+= ce.getProteinas();
            lipidos+= ce.getLipidos();
        }

        calorias_count.append(" - " + calorias + " kcal");

        entries.add(new PieEntry((float) hidratos , types_nutricao[0]));
        entries.add(new PieEntry((float) proteinas , types_nutricao[1]));
        entries.add(new PieEntry((float) lipidos , types_nutricao[2]));

        PieDataSet dataSet = new PieDataSet(entries, "Nutrition");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.MATERIAL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
       // data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        /**********************************data.setValueTypeface(tf);**********************************/
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();

    }


}
