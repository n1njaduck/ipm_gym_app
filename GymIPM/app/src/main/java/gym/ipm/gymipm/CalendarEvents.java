package gym.ipm.gymipm;

import android.util.Log;

import com.orm.SugarRecord;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class CalendarEvents extends SugarRecord {

    String name;
    String description;
    int year;
    int month;
    int day;
    int hour;
    int min;

    public CalendarEvents(){

    }

    public CalendarEvents(String name, String description, int year, int month, int day, int hour, int min){
        this.name = name;
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.min = min;
    }

    public Date getDate(){

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 12);
        cal.set(Calendar.MINUTE, 12);
        cal.set(Calendar.SECOND, 12);
        cal.set(Calendar.MILLISECOND, 12);

        return cal.getTime();
    }

    public String getType(){
        return name;
    }

    public void setType(String new_type){
        this.name = new_type;
    }

    public int getYear() {
        return year;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public int getMonth() {
        return month;
    }

    @Override
    public Long getId() {
        return super.getId();
    }

}
