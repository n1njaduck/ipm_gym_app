package gym.ipm.gymipm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        CalendarEvents aula_grupo = new CalendarEvents("aula_grupo","Aula de Grupo",
                2016, 10, 30, 13, 15);
        aula_grupo.save();

        Alimento a1 = new Alimento("Frango", 250,45,77,20);
        a1.save();

        final Button button = (Button) findViewById(R.id.button_login);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final TextView username_textview = (TextView) findViewById(R.id.editText);
                final TextView password_textview = (TextView) findViewById(R.id.editText2);

                String username = username_textview.getText().toString();
                String password = password_textview.getText().toString();

                if((username.equals("ze") && password.equals("123")) ||
                        (username.equals("mygym") && password.equals("mygym")) ) {
                    Intent i = new Intent(getApplicationContext(), Dashboard.class);
                    i.putExtra("username", username);
                    i.putExtra("password", password);
                    startActivity(i);

                }
            }
        });

    }
}
