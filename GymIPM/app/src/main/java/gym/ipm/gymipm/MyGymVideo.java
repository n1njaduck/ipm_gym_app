package gym.ipm.gymipm;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class MyGymVideo extends Fragment {

    public MyGymVideo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.mygym_video360_fragment, container, false);

        final Button button_playvideo = (Button) view.findViewById(R.id.button_playvideo);
        button_playvideo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=g2bxA3CxekA&t=10s"));
                intent.putExtra("force_fullscreen",true);
                startActivity(intent);
            }
        });

    return view;

    }
}
