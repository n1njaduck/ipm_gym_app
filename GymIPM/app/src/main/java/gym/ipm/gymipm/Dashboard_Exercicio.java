package gym.ipm.gymipm;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Dashboard_Exercicio extends Fragment {


    private ListView listView;

    public Dashboard_Exercicio() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard__exercicio, container, false);

        listView = (ListView) view.findViewById(R.id.list_de_exercicios2);

        ArrayList<String> values2 = new ArrayList<String>();

        List<Exercise> exe = Exercise.listAll(Exercise.class);

        Log.d("SIZE", Integer.toString(exe.size()));

        for(int i=0; i< exe.size(); i++){

            String final_string;
            String name = exe.get(i).getName();

            if(name.equalsIgnoreCase("Cardio")){
                final_string = name + " - Duração: " + exe.get(i).getDuracao() + " minutos";
            } else {
                final_string = name + " - Séries: " + exe.get(i).getSeries() + " / Repetições: " + exe.get(i).getRepeticoes() + " / Kilos: " + exe.get(i).getQuilos() + "kg";
            }

            values2.add(final_string);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, values2);

        listView.setAdapter(adapter);

        LineChart lineChart = (LineChart) view.findViewById(R.id.chart2);

        ArrayList<Entry> values = new ArrayList<Entry>();

        values.add(new Entry(0, 80));
        values.add(new Entry(1, 83));
        values.add(new Entry(2, 85));
        values.add(new Entry(3, 80));
        values.add(new Entry(4, 87));
        values.add(new Entry(5, 85));
        values.add(new Entry(6, 80));
        values.add(new Entry(7, 78));
        values.add(new Entry(8, 75));
        values.add(new Entry(9, 76));
        values.add(new Entry(10, 78));
        values.add(new Entry(11, 78));
        values.add(new Entry(12, 81));
        values.add(new Entry(13, 79));
        values.add(new Entry(14, 73));
        values.add(new Entry(15, 78));
        values.add(new Entry(16, 80));
        values.add(new Entry(17, 80));

        LineDataSet set1 = new LineDataSet(values, "Peso");
        set1.enableDashedLine(10f, 5f, 0f);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);
        set1.setFormLineWidth(1f);
       // set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setFillColor(Color.GRAY);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets
        LineData data = new LineData(dataSets);

        lineChart.animateX(2000);
        lineChart.animateY(2000);
        lineChart.setData(data);

        return view;
    }

}
