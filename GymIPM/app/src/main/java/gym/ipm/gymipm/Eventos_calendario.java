package gym.ipm.gymipm;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.clans.fab.FloatingActionButton;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Eventos_calendario extends AppCompatActivity {

    private Toolbar toolbar;
    SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dateFormatter  = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Adicionar evento");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(i);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Evento.class);
                startActivity(i);
            }
        });

        calenderFragment();

    }

    private void calenderFragment() {

        CaldroidFragment caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
        caldroidFragment.setArguments(args);


        CalendarEvents rotina = new CalendarEvents("rotina","Consulta de Rotina",
                2016, 10, 10, 13, 15);
        rotina.save();

        List<CalendarEvents> cal_events = CalendarEvents.listAll(CalendarEvents.class);
        for(CalendarEvents ce: cal_events){

            if(ce.getType().equals("massagem")){
                ColorDrawable massagem_color = new ColorDrawable(Color.rgb(255,165,52));
                caldroidFragment.setBackgroundDrawableForDate(massagem_color, ce.getDate() );

            }else if(ce.getType().equals("rotina")){
                ColorDrawable rotina_color = new ColorDrawable(Color.rgb(0,153,51));
                caldroidFragment.setBackgroundDrawableForDate(rotina_color, ce.getDate() );

            }else if(ce.getType().equals("aula_grupo")){
                ColorDrawable rotina_color = new ColorDrawable(Color.rgb(0,153,255));
                caldroidFragment.setBackgroundDrawableForDate(rotina_color, ce.getDate() );

            }else if(ce.getType().equals("changed")){
                ColorDrawable changed_color = new ColorDrawable(Color.BLACK);
                caldroidFragment.setBackgroundDrawableForDate(changed_color, ce.getDate() );
            }
        }




        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                String c_year = String.valueOf(calendar.get(Calendar.YEAR));
                String c_month = String.valueOf(calendar.get(Calendar.MONTH));
                String c_day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));

                List<CalendarEvents> list = CalendarEvents.find(CalendarEvents.class, "year = ? and month = ? and day= ?",
                        c_year, c_month, c_day);

                if(list.size()>0){

                    CalendarEvents ce = list.get(0);

                    if(ce.getType().equals("massagem")){
                        Intent i = new Intent(getApplicationContext(), Evento_MassagemDetalhe.class);
                        i.putExtra("id", ce.getId());

                        // date
                        i.putExtra("year", String.valueOf(ce.getYear()));
                        i.putExtra("month", String.valueOf(ce.getMonth()));
                        i.putExtra("day", String.valueOf(ce.getDay()));

                        // hour/min
                        i.putExtra("hour", String.valueOf(ce.getHour()));
                        i.putExtra("minutes", String.valueOf(ce.getMin()));

                        startActivity(i);

                    } else if(ce.getType().equals("aula_grupo")){
                        Intent i = new Intent(getApplicationContext(), Evento_AulaGrupo.class);
                        i.putExtra("id", ce.getId() );
                        startActivity(i);

                    } else if(ce.getType().equals("rotina")){
                        Intent i = new Intent(getApplicationContext(), Evento_RotinaDetalhe.class);
                        i.putExtra("id", ce.getId() );
                        startActivity(i);
                    }

                }

            }

            @Override
            public void onChangeMonth(int month, int year) {

            }

            @Override
            public void onLongClickDate(Date date, View view) {

            }

            @Override
            public void onCaldroidViewCreated() {

            }

        };

        caldroidFragment.setCaldroidListener(listener);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar_eventos, caldroidFragment);
        t.commit();
    }
}
