package gym.ipm.gymipm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class CodeBarRelay extends AppCompatActivity {

    private Button button;
    private TextView edit_nome;
    private TextView edit_calorias;
    private TextView edit_hidratos;
    private TextView edit_lipidos;
    private TextView edit_proteina;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_bar_relay);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        IntentIntegrator scanner = new IntentIntegrator(this);
        scanner.setWide();
        scanner.setPrompt("Ler Código de Barras");
        scanner.initiateScan();

        findIds();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Alimento a1 = new Alimento("Iorgurte Danone", 54, (float)7.7 ,  (float)0.5,  (float)3.6);
                a1.save();

                Intent i = new Intent(getBaseContext(),Dashboard.class);
                i.putExtra("viewpager_position", 1);
                startActivity(i);
            }
        });
    }

    private void findIds() {
        button = (Button) findViewById(R.id.button_confirmar_registo);
        /*edit_nome = (EditText) findViewById(R.id.editText3);
        edit_calorias = (EditText) findViewById(R.id.editText10);
        edit_hidratos = (EditText) findViewById(R.id.editText11);
        edit_lipidos = (EditText) findViewById(R.id.editText12);
        edit_proteina = (EditText) findViewById(R.id.editText14);*/

        edit_nome = (TextView) findViewById(R.id.editText3);
        edit_calorias = (TextView) findViewById(R.id.editText10);
        edit_hidratos = (TextView) findViewById(R.id.editText11);
        edit_lipidos = (TextView) findViewById(R.id.editText12);
        edit_proteina = (TextView) findViewById(R.id.editText14);
        image = (ImageView) findViewById(R.id.imageView7);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();

           // if(scanContent.equals("5601050031270")){
                edit_nome.setText("Iorgute Pinã Colada");
                edit_calorias.setText("54 kcal");
                edit_hidratos.setText("7,7 gramas");
                edit_lipidos.setText("0,5 gramas");
                edit_proteina.setText("3,6 gramas");
                image.setImageResource(R.drawable.iorgurte);



        }else{
            edit_nome.setText("ERRO A LER CODIGO DE BARRAS");
            edit_calorias.setText("ERRO A LER CODIGO DE BARRAS");
            edit_hidratos.setText("ERRO A LER CODIGO DE BARRAS");
            edit_lipidos.setText("ERRO A LER CODIGO DE BARRAS");
            edit_proteina.setText("ERRO A LER CODIGO DE BARRAS");

        }

    }

}
