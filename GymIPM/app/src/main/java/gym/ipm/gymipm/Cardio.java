package gym.ipm.gymipm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ToggleButton;

public class Cardio extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardio);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Adicionar Cardio");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Exercicio.class);
                startActivity(i);
            }

        });

        final EditText duracao = (EditText) findViewById(R.id.editText4);
        final EditText intensidade = (EditText) findViewById(R.id.editText5);

        final ToggleButton aquecimento = (ToggleButton) findViewById(R.id.toggleButton);
        final ToggleButton treino_final = (ToggleButton) findViewById(R.id.toggleButton2);
        Button adicionar = (Button) findViewById(R.id.button3);


        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder( Cardio.this);

                builder.setMessage("Adicionar exercício?")
                        .setPositiveButton("Sim",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                String type;

                                if(aquecimento.isChecked()){
                                    type = "Aquecimento";

                                }else if(treino_final.isChecked()){
                                    type = "Treino Final";

                                }else{
                                    type = "Aquecimento";
                                }



                                Log.d("DURACAO",duracao.getText().toString());
                                Exercise a1 = new Exercise("Cardio", Integer.valueOf(duracao.getText().toString() ),
                                        Integer.valueOf(intensidade.getText().toString() ),
                                        type);
                                a1.save();

                                Intent i = new Intent(getApplicationContext(), Dashboard.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })
                        .show();






            }
        });

        aquecimento.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                treino_final.setChecked(false);
            }
        });

        treino_final.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                aquecimento.setChecked(false);
            }
        });
    }
}
