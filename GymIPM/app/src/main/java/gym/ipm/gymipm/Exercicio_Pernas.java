package gym.ipm.gymipm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Exercicio_Pernas extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercicio__pernas);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Adicionar Ex. Pernas");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Exercicio.class);
                startActivity(i);
            }
        });


        final EditText series = (EditText) findViewById(R.id.edit_pernas_series);
        final EditText reps = (EditText) findViewById(R.id.editText6);
        final EditText quilos = (EditText) findViewById(R.id.editText7);

        Button button = (Button) findViewById(R.id.addExercicioPernas);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder( Exercicio_Pernas.this);

                builder.setMessage("Adicionar exercício?")
                        .setPositiveButton("Sim",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                            Exercise a1 = new Exercise("Pernas", Integer.valueOf(series.getText().toString() ),
                                    Integer.valueOf(reps.getText().toString() ),
                                    Integer.valueOf(quilos.getText().toString() ));

                                a1.save();

                                Intent i = new Intent(getApplicationContext(), Dashboard.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })
                        .show();


            }
        });


    }
}
