package gym.ipm.gymipm;

import com.orm.SugarRecord;

/**
 * Created by Douglas on 24/11/2016.
 */

public class Exercise extends SugarRecord {

    private String type;
    private String name;
    private int series;
    private int repeticoes;
    private int quilos;
    
    private int duracao;
    private int intensidade;

    public Exercise(){

    }

    public Exercise(String name, int series, int repeticoes, int quilos){
        this.name = name;
        this.series = series;
        this.repeticoes = repeticoes;
        this.quilos = quilos;
    }

    public Exercise(String name, int duracao, int intensidade, String type){
        this.name = name;
        this.duracao = duracao;
        this.intensidade = intensidade;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getDuracao() {
        return duracao;
    }

    public int getIntensidade() {
        return intensidade;
    }

    public int getRepeticoes() {
        return repeticoes;
    }

    public int getQuilos() {
        return quilos;
    }

    public int getSeries() {
        return series;
    }
}
