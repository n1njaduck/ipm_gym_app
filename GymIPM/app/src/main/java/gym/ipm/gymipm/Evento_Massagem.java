package gym.ipm.gymipm;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Evento_Massagem extends AppCompatActivity implements View.OnClickListener {

    private int mYear;
    private int mMonth;
    private int mDay;
    private int mHour;
    private int mMinutes;

    private EditText date;
    private EditText time;

    private DatePickerDialog dateDialog;
    private TimePickerDialog timeDialog;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat timeFormatter;
    private Button criar_button;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massagem);

        // toolbar props
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Adicionar nova massagem");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Evento.class);
                startActivity(i);
            }
        });

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        timeFormatter = new SimpleDateFormat("HH:mm", Locale.US);

        findViewsById();

        setDateTimeField();
        setTimeField();

        setCriarEvento();
    }

    private void setCriarEvento() {

        criar_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder( Evento_Massagem.this);

                builder.setMessage("Confirmar evento?")
                        .setPositiveButton("Sim",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                CalendarEvents ce = new CalendarEvents("massagem","Evento_Massagem para efeitos de reabilitação",
                                        mYear, mMonth, mDay, mHour, mMinutes);
                                ce.save();

                                Log.d("mMonth", String.valueOf(mMonth));
                                Intent i = new Intent(getApplicationContext(), Eventos_calendario.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })
                        .show();


            }
        });
    }

    @Override
    public void onClick(View v) {

        if(v == date) {
            dateDialog.show();

        }else if(v == time) {
            timeDialog.show();

        }

    }

    private void setDateTimeField() {
        date.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        dateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {


            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(dateFormatter.format(newDate.getTime()));

                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }
    private void setTimeField() {
        time.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        timeDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                newDate.set(Calendar.MINUTE, minute);
                time.setText(timeFormatter.format(newDate.getTime()));

                mHour = hourOfDay;
                mMinutes = minute;

            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);

    }

    private void findViewsById() {
        date = (EditText) findViewById(R.id.editText_date);
        date.setInputType(InputType.TYPE_NULL);


        time = (EditText) findViewById(R.id.editText_horas);
        time.setInputType(InputType.TYPE_NULL);

        criar_button = (Button) findViewById(R.id.button_create_event);

    }

}
