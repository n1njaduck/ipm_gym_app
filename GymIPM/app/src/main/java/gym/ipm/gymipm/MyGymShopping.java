package gym.ipm.gymipm;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyGymShopping extends Fragment {


    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public MyGymShopping() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.mygym_shopping_fragment, container, false);
        
        viewPager = (ViewPager) view.findViewById(R.id.viewpager_shopping);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs_shooping);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        
        return view;
    }

    private void setupTabIcons() {

        tabLayout.getTabAt(0).setText("T-shirts");
        tabLayout.getTabAt(1).setText("Mochilas");
        tabLayout.getTabAt(2).setText("Casacos");

    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Shopping_Tshirts(), "T-shirts");
        adapter.addFragment(new Shopping_Mochilas(), "Mochilas");
        adapter.addFragment(new Shopping_casacos(), "Casacos");

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
