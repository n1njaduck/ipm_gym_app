package gym.ipm.gymipm;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class CodeBar extends Fragment {

    public CodeBar() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_code_bar, container, false);

        final Button button_codigo = (Button) view.findViewById(R.id.button_codigo_barras);

        button_codigo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(CodeBar.this.getContext(), CodeBarRelay.class);
                startActivity(i);
            }
        });

        return view;
    }
}
