package gym.ipm.gymipm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Evento_MassagemDetalhe extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento__massagem_detalhe);

        // toolbar props
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Detalhe Massagem");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Eventos_calendario.class);
                startActivity(i);
            }
        });

        Intent intent = getIntent();

        final TextView massagemData = (TextView) findViewById(R.id.massagemData);
        massagemData.setText( intent.getStringExtra("day") + "/" + intent.getStringExtra("month") + "/" + intent.getStringExtra("year") );

        final TextView massagemHoras = (TextView) findViewById(R.id.massagemHoras);
        massagemHoras.setText( intent.getStringExtra("hour")  + ":" + intent.getStringExtra("minutes") );

    }
}
